package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"os"
)

func main() {
	if len(os.Args) > 1 && isHelp(os.Args[1]) {
		usage(os.Stdout)
		os.Exit(0)
	}

	if len(os.Args) < 2 {
		fatal("ERROR: not enough arguments")
	}

	path := os.Args[1]
	sum := calculateSum(path)

	if len(os.Args) >= 3 {
		hash := os.Args[2]

		fmt.Printf("\n\thash:\t%s\n", hash)
		fmt.Printf("\tsum:\t%s\n\n", sum)

		if sum == hash {
			fmt.Printf("\tmatch\n\n")
			os.Exit(0)
		} else {
			fmt.Fprintf(os.Stderr, "\tdo not match\n\n")
			os.Exit(1)
		}
	} else {
		fmt.Println(sum)
	}
}

func isHelp(arg string) bool {
	return arg == "-h" || arg == "help"
}

func usage(w io.Writer) {
	fmt.Fprintln(w, "Usage: sha <source> [hash]")
	fmt.Fprintln(w, "  source:\n    - path to a file\n    - \"-\" stdin")
	fmt.Fprintln(w, "  hash: optional validation sha256sum")
}

func fatal(msg string) {
	fmt.Fprintln(os.Stderr, msg)
	usage(os.Stderr)
	os.Exit(1)
}

func fatalf(format string, args ...any) {
	fmt.Fprintf(os.Stderr, format, args...)
	usage(os.Stderr)
	os.Exit(1)
}

func openInputFile(path string) (*os.File, error) {
	if path == "-" {
		return os.Stdin, nil
	}

	return os.Open(path)
}

func calculateSum(path string) string {
	f, err := openInputFile(path)
	if err != nil {
		fatalf("ERROR: failed to open input file for reading: %s", err)
	}

	defer f.Close()

	hash := sha256.New()

	_, err = io.Copy(hash, f)
	if err != nil {
		fatalf("ERROR: failed to copy input file to hash buffer: %s", err)
	}

	err = f.Close()
	if err != nil {
		fatalf("ERROR: failed to close input file: %s", err)
	}

	sum := hash.Sum(nil)

	return fmt.Sprintf("%x", sum)
}
