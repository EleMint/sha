# SHA265 Utility

Lightweight utility for calculating `sha265sum`

## Usage
```sh
sha <source> [hash]

  source:
    - path to a file
    - '-' stdin
  
  hash: optional validation sha256sum
```

## Generate Sum

### From File

```sh
$ wget https://go.dev/dl/go1.20.linux-amd64.tar.gz
$ sha go1.20.linux-amd64.tar.gz
```

Output
```
5a9ebcc65c1cce56e0d2dc616aff4c4cedcfbda8cc6f0288cc08cda3b18dcbf1
```

### From Stdin

```sh
$ wget https://go.dev/dl/go1.20.linux-amd64.tar.gz
$ cat go1.20.linux-amd64.tar.gz | sha -
```

Output
```
5a9ebcc65c1cce56e0d2dc616aff4c4cedcfbda8cc6f0288cc08cda3b18dcbf1
```

## Validate

Output
```
  hash:  <provided hash>
  sum:   <hashed source>

  [match | do not match]
```

### File

```sh
$ wget https://go.dev/dl/go1.20.linux-amd64.tar.gz
$ sha go1.20.linux-amd64.tar.gz 5a9ebcc65c1cce56e0d2dc616aff4c4cedcfbda8cc6f0288cc08cda3b18dcbf1
```


### Stdin

```sh
$ wget https://go.dev/dl/go1.20.linux-amd64.tar.gz
$ cat go1.20.linux-amd64.tar.gz | sha - 5a9ebcc65c1cce56e0d2dc616aff4c4cedcfbda8cc6f0288cc08cda3b18dcbf1
```
